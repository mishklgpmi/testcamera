﻿#include <iostream>
#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include "rectangle.hpp"

using namespace std::chrono_literals;
int main()
{
    const float PI = acos(-1);

    ts::Camera cam(800,600,{0,0,0},{0,0,0}, {200,100,400,300});

    sf::RenderWindow wnd(sf::VideoMode(cam.Width(), cam.Height()), "SFML");
    wnd.setFramerateLimit(60);
    sf::Texture tx;
    tx.create(cam.Width(), cam.Height());
    sf::Sprite sprite(tx);

    while (wnd.isOpen()) {
        sf::Event event;
        while (wnd.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                wnd.close();
                break;
            }
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
        {
            cam.dZ(-0.1);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
        {
            cam.dZ(0.1);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            cam.dPitch(0.1);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            cam.dPitch(-0.1);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            cam.dRoll(0.1);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            cam.dRoll(-0.1);
        }

        cam.Clear();
        for(float i=-5;i<=5;i+=0.01)
            for(float j=-5;j<=5;j+=0.01) {
                cam.ProjectPoint({(float) i, (float) j, 3}, {0,255,0,128});
                //cam.ProjectPoint({1, (float) i, (float) j});
            }

        int r = 1;
        for(float i=0;i<PI/2;i+=0.01)
            for(float j=0;j<2*PI;j+=0.01)
                cam.ProjectPoint({r*sin(i)*cos(j), r*sin(i)*sin(j), r*cos(i)}, {255,0,0,128});

        tx.update((uint8_t *) cam.Picture(), cam.Width(), cam.Height(), 0, 0);
        sprite.setPosition(0, 0);
        wnd.clear();
        wnd.draw(sprite);
        wnd.display();
    }
}